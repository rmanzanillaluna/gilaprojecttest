import { createRouter, createWebHistory } from 'vue-router'
import VehiclesList from '../views/VehiclesList.vue'
import AddVehicle from '../views/AddVehicle.vue'
import EditVehicle from '../views/EditVehicle.vue'
import Login from '../views/Login.vue'
 
const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login
  },
  {
    path: '/list',
    name: 'VehiclesList',
    component: VehiclesList
  },
  {
    path: '/add',
    name: 'AddVehicle',
    component: AddVehicle
  },
  {
    path: '/edit/:id',
    name: 'EditVehicle',
    component: EditVehicle
  },
]
 
const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})
 
export default router