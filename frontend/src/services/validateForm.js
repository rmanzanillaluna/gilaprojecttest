export default function validateForm(vehicle) {
    
  let errors = [];

  if (!vehicle.name) {
    errors.push('El nombre es obligatorio.');
  }
  if (!vehicle.tires || vehicle.tires <= 0) {
    errors.push('El # de Llantas es obligatoria y mayor a 0.');
  }
  if (!vehicle.hp || vehicle.hp <= 0) {
    errors.push('Los Caballos de Fuerza son obligatorios y mayor a 0.');
  }
  if (!vehicle.id_type_vehicle) {
    errors.push('Debe seleccionar un Tipo.');
  }
  return errors;
}