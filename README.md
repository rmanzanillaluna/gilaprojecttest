# GilaProjectTest

## Backend

Framework - Codeigniter 4

Requerimientos de Servidor
PHP version >= 7.3, con Extension *intl*, *mbstring*, *php-json*, *php-mysqlnd*, *php-xml* instaladas.

Base de Datos
MySQL (5.1+) via driver MySQLi

### Instalacion de Ambiente Local

En el directorio del proyecto ingresar a la carpeta Backend

### Env
Renombrar el archivo *env* a *.env*

### Ubicar las configuraciones, descomentar y editar  en caso de ser necesario
```
app.baseURL = 'http://localhost:8080/' 
app.TOKEN_SECRET = "gilaProjectTestsTokenSecret"
```

### Ubicar las configuraciones, descomentar y editar con la configracion de la BD que montará el proyecto
```
#--------------------------------------------------------------------
# DATABASE
#--------------------------------------------------------------------

database.default.hostname = server
database.default.database = dbname
database.default.username = user
database.default.password = pass
```

### Instalar dependencias
```
composer install

```
### Crear BD

```
php spark db:create dbname
```
### Crear Tablas
```
php spark migrate
```
### Volcar datos en DB
```
php spark db:seed users
php spark db:seed typevehicles
```
### Montar Servidor local
```
php spark serve
```

## Frontend
Elaborado con Vue.js

Instalacion del ambiente Local

Ubicarse dentro de la carpeta del proyecto "Frontend"

### Instalacion usando npm
```
npm install
```

### Compilar e ingresar a servidor Local
```
npm run serve
```