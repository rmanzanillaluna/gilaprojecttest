<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Users extends Seeder
{
    public function run()
    {
        $data = [
            'name'       => 'Gila',
            'last_name'  => 'Software',
            'email'      => 'test@gilasw.mx',
            'pass'       => 'test.gilasw'
        ];

        $data["pass"] = password_hash($data["pass"] ?? "", PASSWORD_BCRYPT);

        $this->db->table('users')->insert($data);
    }
}
