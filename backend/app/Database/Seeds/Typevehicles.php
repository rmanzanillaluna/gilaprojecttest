<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Typevehicles extends Seeder
{
    public function run()
    {
        $data = [
            ['name' => 'motocicletas'],
            ['name' => 'Sedan']
        ];

        $this->db->table('type_vehicles')->insertBatch($data);
    }
}
