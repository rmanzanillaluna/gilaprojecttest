<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class TypeVehicles extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id_type_vehicle'          => [
                'type'           => 'INT',
                'constraint'     => 10,
                'unsigned'       => TRUE,
                'auto_increment' => TRUE
            ],
            'name varchar(100) NOT NULL',
            'created_at datetime DEFAULT CURRENT_TIMESTAMP',
            'updated_at datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP',
            'deleted_at datetime DEFAULT NULL',
        ]);
        $this->forge->addPrimaryKey('id_type_vehicle');
        $this->forge->createTable('type_vehicles', TRUE, ['ENGINE' => 'InnoDB']);
    }

    public function down()
    {
        $this->forge->dropTable('type_vehicles');
    }
}
