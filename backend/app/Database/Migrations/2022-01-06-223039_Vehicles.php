<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Vehicles extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id_vehicle'          => [
                'type'           => 'INT',
                'constraint'     => 10,
                'unsigned'       => TRUE,
                'auto_increment' => TRUE
            ],
            'id_type_vehicle'          => [
                'type'           => 'INT',
                'unsigned'       => TRUE,
            ],
            'name varchar(100) NOT NULL',
            'tires integer NOT NULL',
            'hp numeric(10, 2) NOT NULL',
            'created_at datetime DEFAULT CURRENT_TIMESTAMP',
            'updated_at datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP',
            'deleted_at datetime DEFAULT NULL',
        ]);
        $this->forge->addPrimaryKey('id_vehicle');
        $this->forge->addForeignKey('id_type_vehicle', 'type_vehicles', 'id_type_vehicle');
        $this->forge->createTable('vehicles', TRUE, ['ENGINE' => 'InnoDB']);
    }

    public function down()
    {
        $this->forge->dropTable('vehicles');
    }
}
