<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Users extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id_user'          => [
                'type'           => 'INT',
                'constraint'     => 10,
                'unsigned'       => TRUE,
                'auto_increment' => TRUE
            ],
            'name varchar(100) NOT NULL',
            'last_name varchar(100) NOT NULL',
            'email varchar(100) NOT NULL',
            'pass varchar(100) NOT NULL',
            'created_at datetime DEFAULT CURRENT_TIMESTAMP',
            'updated_at datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP',
            'deleted_at datetime DEFAULT NULL',
        ]);
        $this->forge->addPrimaryKey('id_user');
        $this->forge->createTable('users', TRUE, ['ENGINE' => 'InnoDB']);
    }

    public function down()
    {
        $this->forge->dropTable('users');
    }
}
