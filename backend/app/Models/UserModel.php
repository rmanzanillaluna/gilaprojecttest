<?php

namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{    
	protected $table = 'users';
    protected $primaryKey = 'id_user';

    protected $useAutoIncrement = true;

    protected $returnType    = 'object';
    protected $useSoftDeletes = true;

    // protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

	protected $allowedFields = [
		'name',
        'last_name',
        'email',
        'pass'
	];
}
