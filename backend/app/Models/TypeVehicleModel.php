<?php

namespace App\Models;

use CodeIgniter\Model;

class TypeVehicleModel extends Model
{
	protected $table = 'type_vehicles';
    protected $primaryKey = 'id_type_vehicle';

    protected $useAutoIncrement = true;

    // protected $returnType    = 'object';
    protected $returnType    = 'object';
    protected $useSoftDeletes = true;

    // protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

	protected $allowedFields = [
		'id_type_vehicle',
        'name'
	];
}
