<?php

namespace App\Models;

use CodeIgniter\Model;

class VehicleModel extends Model
{
	protected $table = 'vehicles';
    protected $primaryKey = 'id_vehicle';

    protected $useAutoIncrement = true;

    // protected $returnType    = 'object';
    protected $returnType    = 'App\Entities\Vehicle';
    protected $useSoftDeletes = true;

    // protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

	protected $allowedFields = [
		'id_vehicle',
        'id_type_vehicle',
        'name',
        'tires',
        'hp'
	];

    
	public function get_vehicles_list()
	{
		$builder = $this->table('vehicles');
		$builder->select('vehicles.id_vehicle, vehicles.name, vehicles.hp, vehicles.tires, vehicles.id_type_vehicle, tv.name as type_vehicle');
		$builder->join('type_vehicles AS tv', 'tv.id_type_vehicle = vehicles.id_type_vehicle');
		return $builder->findAll();
	}
}
