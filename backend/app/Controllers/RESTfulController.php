<?php

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use Exception;
use \Firebase\JWT\JWT;

class RESTfulController extends ResourceController
{
    protected function security_create($aobj_user)
    {
        $key = getenv("app.TOKEN_SECRET");

        $iat = time();
        $nbf = $iat + 1;
        $exp = $iat + 600;

        $payload = array(
            "iat" => $iat,
            "nbf" => $nbf, 
            "exp" => $exp,
            "data" => $aobj_user,
        );

        return JWT::encode($payload, $key);
    }

    protected function validate_request($aar_rules, &$aar_response) : bool
    {
        $validation =  \Config\Services::validation();

        $validation->setRules($aar_rules);

        if (!$validation->withRequest($this->request)->run()) {
            $aar_response = $validation->getErrors();
            return false;
        }

        return true;
    }
}
