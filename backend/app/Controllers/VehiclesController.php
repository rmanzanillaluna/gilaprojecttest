<?php

namespace App\Controllers;


use App\Controllers\RESTfulController;
use App\Models\TypeVehicleModel;
use App\Models\VehicleModel;

class VehiclesController extends RESTfulController
{
    private $Vehicles;
    private $TypeVehicle;

    function __construct()
    {
        // header("Access-Control-Allow-Origin: *");
        $this->Vehicles = new VehicleModel();
    }
    /**
     * Listado
     *
     * @return mixed
     */
    public function index()
    {
        $rows = $this->Vehicles->get_vehicles_list();
        return $this->setResponseFormat('json')->respond($rows);
    }

    /**
     * Elemento
     *
     * @return mixed
     */
    public function show($id = null)
    {
        $vehicle = $this->Vehicles->find($id);
        return $this->setResponseFormat('json')->respond($vehicle);
    }

    /**
     * Guardar
     *
     * @return mixed
     */
    public function create()
    {
        $rules = [
            "id_type_vehicle" => "required|integer",
            "name"      => "required|string|min_length[3]",
            "tires"     => "required|integer|greater_than[0]",
            "hp"        => "required|decimal|greater_than[0]",
        ];

        if (!$this->validate_request($rules, $response)) {
            return $this->failValidationErrors($response);
        }

        $vehicle                    = new \App\Entities\Vehicle();
        $vehicle->id_type_vehicle   = $this->request->getVar("id_type_vehicle");
        $vehicle->name              = $this->request->getVar("name");
        $vehicle->tires             = $this->request->getVar("tires");
        $vehicle->hp                = $this->request->getVar("hp");

        if (!$this->Vehicles->save($vehicle)) {
            // return $this->fail($this->Vehicles->errors(), 400);
            
            $this->failValidationErrors($this->Vehicles->errors());
        }
        $vehicle->id_vehicle = $this->Vehicles->insertID();
        
        return $this->respondCreated($vehicle);
    }

    /**
     * Actualizar
     *
     * @return mixed
     */
    public function update($id = null)
    {
        $vehicle = $this->Vehicles->find($id);
        if(empty($vehicle))
        {
            return $this->failNotFound();
        }
        
        $rules = [
            "id_type_vehicle" => "required|integer",
            "name"      => "required|string|min_length[3]",
            "tires"     => "required|integer|greater_than[0]",
            "hp"        => "required|decimal|greater_than[0]",
        ];

        if (!$this->validate_request($rules, $response)) {
            return $this->failValidationErrors($response);
        }
        
        $vehicle->id_type_vehicle   = $this->request->getVar("id_type_vehicle");
        $vehicle->name              = $this->request->getVar("name");
        $vehicle->tires             = $this->request->getVar("tires");
        $vehicle->hp                = $this->request->getVar("hp");

        if (!$vehicle->hasChanged()) {
			return $this->respond("Sin cambios pendientes para guardar", 200, "Sin cambios pendientes para guardar");
		}

        if (!$this->Vehicles->save($vehicle)) {
            $this->failValidationErrors($this->Vehicles->errors());
        }

        return $this->setResponseFormat('json')->respond($vehicle);
    }

    /**
     * Eliminar
     *
     * @return mixed
     */
    public function delete($id = null)
    {
        $vehicle = $this->Vehicles->find($id);
        if(empty($vehicle))
        {
            return $this->failNotFound();
        }

        if(!$this->Vehicles->delete($id))
        {
            return $this->fail($this->Vehicles->errors(), 400);
        }

        return $this->respondDeleted($vehicle);
    }

    /**
     * Listado
     *
     * @return mixed
     */
    public function typeVehicles()
    {
        $this->TypeVehicle = new TypeVehicleModel();
        $rows = $this->TypeVehicle->findAll();
        return $this->setResponseFormat('json')->respond($rows);
    }
}
