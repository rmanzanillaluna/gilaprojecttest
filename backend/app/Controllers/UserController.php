<?php

namespace App\Controllers;

use App\Controllers\RESTfulController;
use App\Models\UserModel;

class UserController extends RESTfulController
{

    private $User;

    function __construct()
    {
        $this->User = new UserModel();
    }

    public function register()
    {
        $rules = [
            "name"      => "required",
            "last_name" => "required",
            "email"     => "required|valid_email|is_unique[users.email]|min_length[6]",
            "password"  => "required",
        ];

        if (!$this->validate_request($rules, $response)) {
            return $this->failValidationErrors($response);
        }

        $user = [
            "name" => $this->request->getVar("name"),
            "email" => $this->request->getVar("email"),
            "last_name" => $this->request->getVar("last_name"),
            "pass" => password_hash($this->request->getVar("password"), PASSWORD_DEFAULT),
        ];
        
        if (!$this->User->save($user)) {
            $this->failValidationErrors($this->User->errors());
        }
        unset($user["pass"]);
        return $this->respondCreated($user);
    }

    public function login()
    {
        $rules = [
            "email" => "required|valid_email|min_length[6]",
            "password" => "required",
        ];

        if (!$this->validate_request($rules, $response)) {
            return $this->failValidationErrors($response);
        }

        $userdata = $this->User->where("email", $this->request->getVar("email"))->first();

        if (empty($userdata)) {
            return $this->failNotFound();
        }
        if (!password_verify($this->request->getVar("password"), $userdata->pass)) {
            return $this->failUnauthorized('Datos incorrectos');
        }

        $token = $this->security_create($userdata);
        return $this->respond(["user" => $userdata->name, "token" => $token], 200);
    }
}
