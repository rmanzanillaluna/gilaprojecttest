<?php

namespace App\Filters;

use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Exception;
use \Firebase\JWT\JWT;
use CodeIgniter\API\ResponseTrait;
// use App\Filters\Respose

class ApiV1Auth implements FilterInterface
{
    use ResponseTrait;
    /**
     * Do whatever processing this filter needs to do.
     * By default it should not return anything during
     * normal execution. However, when an abnormal state
     * is found, it should return an instance of
     * CodeIgniter\HTTP\Response. If it does, script
     * execution will end and that Response will be
     * sent back to the client, allowing for error pages,
     * redirects, etc.
     *
     * @param RequestInterface $request
     * @param array|null       $arguments
     *
     * @return mixed
     */
    public function before(RequestInterface $request, $arguments = null)
    {
        $key = getenv("app.TOKEN_SECRET");
        $response = "";
        try {
            // $authHeader = $request->getHeader("Authorization");
            $authHeader = $request->getServer('HTTP_AUTHORIZATION');
            if (empty($authHeader)) {
                throw new Exception("", 1);
            }
            
            // $authHeader = $authHeader->getValue();
            $token = explode(' ', $authHeader)[1];

            $decoded = JWT::decode($token, $key, array("HS256"));
            if (empty($decoded)) {
                throw new Exception("", 1);
            }
        } catch (Exception $ex) {
            header("Access-Control-Allow-Origin: *");
            header("HTTP/1.0 401 Unauthorized");
            die;
        }
    }

    /**
     * Allows After filters to inspect and modify the response
     * object as needed. This method does not allow any way
     * to stop execution of other after filters, short of
     * throwing an Exception or Error.
     *
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array|null        $arguments
     *
     * @return mixed
     */
    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        //
    }
}
